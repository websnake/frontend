import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { routesCore } from './modules/core/core-routing.module';

const routes: Routes = [{ path: 'core', children: routesCore }, { path: '**', redirectTo: 'core' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
