import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { ControlsComponent } from './pages/controls/controls.component';
import { DifficultyComponent } from './pages/difficulty/difficulty.component';
import { WonComponent } from './pages/won/won.component';
import { LostComponent } from './pages/lost/lost.component';
import { MultiplayerComponent } from './pages/multiplayer/multiplayer.component';

export const routesCore: Routes = [
  { path: 'main', component: MainComponent, data: { animation: 1 } },
  { path: 'controls', component: ControlsComponent, data: { animation: 2 } },
  { path: 'difficulty', component: DifficultyComponent, data: { animation: 3 } },
  { path: 'multiplayer', component: MultiplayerComponent, data: { animation: 4 } },
  { path: 'won', component: WonComponent, data: { animation: 5 } },
  { path: 'lost', component: LostComponent, data: { animation: 6 } },
  { path: '**', redirectTo: 'main' }
];

@NgModule({
  imports: [RouterModule.forRoot(routesCore)],
  exports: [RouterModule]
})
export class CoreRoutingModule {}
