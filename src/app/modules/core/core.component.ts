import { Component, OnInit } from '@angular/core';
import { trigger, transition } from '@angular/animations';
import { slideVertical } from 'src/app/shared/animations/slide/slide';

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss'],
  animations: []
})
export class CoreComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
