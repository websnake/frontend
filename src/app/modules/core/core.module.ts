import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponent } from './core.component';
import { ControlsComponent } from './pages/controls/controls.component';
import { MainComponent } from './pages/main/main.component';
import { CoreRoutingModule } from './core-routing.module';
import { MatButtonModule, MatCardModule, MatTableModule } from '@angular/material';
import { DifficultyComponent } from './pages/difficulty/difficulty.component';
import { LostComponent } from './pages/lost/lost.component';
import { WonComponent } from './pages/won/won.component';
import { MultiplayerComponent } from './pages/multiplayer/multiplayer.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [CoreComponent, ControlsComponent, MainComponent, DifficultyComponent, LostComponent, WonComponent, MultiplayerComponent],
  imports: [CommonModule, CoreRoutingModule, MatButtonModule, MatCardModule, MatTableModule, HttpClientModule]
})
export class CoreModule {}
