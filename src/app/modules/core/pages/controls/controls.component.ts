import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.scss']
})
export class ControlsComponent implements OnInit {
  displayedColumns: string[] = ['name', 'key'];
  dataSource = [];
  constructor() {}

  ngOnInit() {}
}
