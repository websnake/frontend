import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/core/services/game/game.service';

@Component({
  selector: 'app-lost',
  templateUrl: './lost.component.html',
  styleUrls: ['./lost.component.scss'],
})
export class LostComponent implements OnInit {
  constructor(public game: GameService) {}

  ngOnInit() {}
}
