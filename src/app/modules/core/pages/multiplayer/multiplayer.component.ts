import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { GameService } from 'src/app/core/services/game/game.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-multiplayer',
  templateUrl: './multiplayer.component.html',
  styleUrls: ['./multiplayer.component.scss'],
})
export class MultiplayerComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'player'];
  dataSource = new MatTableDataSource();

  constructor(private game: GameService, private http: HttpClient) {}

  async ngOnInit() {
    this.dataSource.data = await this.http.get<any[]>((environment as any).backendUrl || 'https://snake.nkls.net/api/').toPromise();
  }

  startMultiplayer(id: number) {
    this.game.onStart.next({ type: 'multiplayer', data: { id } });
  }
}
