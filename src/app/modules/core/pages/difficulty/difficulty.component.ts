import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/core/services/game/game.service';

@Component({
  selector: 'app-difficulty',
  templateUrl: './difficulty.component.html',
  styleUrls: ['./difficulty.component.scss']
})
export class DifficultyComponent implements OnInit {
  constructor(public game: GameService) {}

  ngOnInit() {}

  startSingleplayer(difficulty: 'easy' | 'medium' | 'hard') {
    this.game.onStart.next({ type: 'singleplayer', data: { difficulty } });
  }
}
