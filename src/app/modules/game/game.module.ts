import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

import { GameRoutingModule } from './game-routing.module';
import { GameComponent } from './game.component';
import { environment } from 'src/environments/environment';

const config: SocketIoConfig = {
  url: (environment as any).backendUrl || 'wss://snake.nkls.net',
  options: {
    transports: ['websocket'],
    path: (environment as any).backendUrl ? '/socket.io' : '/api/socket.io',
  },
};

@NgModule({
  declarations: [GameComponent],
  imports: [CommonModule, GameRoutingModule, SocketIoModule.forRoot(config)],
  exports: [GameComponent],
})
export class GameModule {}
