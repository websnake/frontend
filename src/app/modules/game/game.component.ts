import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Renderer } from 'src/app/shared/game/renderer';
import { Snake, OnlinePlayer } from 'src/app/shared/game/snake';
import { World } from 'src/app/shared/game/world';
import { GameService } from 'src/app/core/services/game/game.service';
import { Socket } from 'ngx-socket-io';
import { Init } from 'src/app/shared/models/init/init';
import { PowerUp } from 'src/app/shared/game/power-up';
import { SnakePart } from 'src/app/shared/game/snake-part';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent {
  @ViewChild('gameCanvas', { static: false }) canvas;
  @ViewChild('gameContainer', { static: false }) container;

  constructor(private gameSerive: GameService, private socket: Socket, private router: Router) {
    let gameType: 'singleplayer' | 'multiplayer';

    gameSerive.onStart.subscribe(config => {
      gameType = config.type;
      if (config.type === 'multiplayer') {
        this.startMultiplayer((config.data as any).id);
      }
    });

    socket.connect();
  }

  async startMultiplayer(id: number) {
    const renderer = new Renderer(this.canvas.nativeElement, this.container.nativeElement);

    this.socket.emit('join', id);
    const { world, self } = await this.socket.fromOneTimeEvent<Init>('init');

    const oWorld = new World(world.width, world.height, new Map(), world.powerUps.map(value => new PowerUp(value.x, value.y, value.type)));

    world.playerList.forEach(player => {
      oWorld.playerList.set(player.id, new OnlinePlayer(player));
    });

    const snake = new Snake(self.id, self.x, self.y, self.direction, self.tailParts, self.speed, this.socket);
    snake.addControls();
    oWorld.setSelf(snake);

    this.socket.fromEvent<OnlinePlayer>('join').subscribe(player => {
      oWorld.playerList.set(player.id, new OnlinePlayer(player));
    });

    this.socket.fromEvent<string>('leave').subscribe(playerId => {
      oWorld.playerList.delete(playerId);
    });

    this.socket.fromEvent<OnlinePlayer>('c03').subscribe(player => {
      const offlinePlayer = oWorld.playerList.get(player.id);
      Object.assign(offlinePlayer, player);
      offlinePlayer.tailParts = player.tailParts.map(tailPart => new SnakePart(tailPart.x, tailPart.y, tailPart.direction));
    });

    this.socket.fromEvent<PowerUp[]>('updatePowerUps').subscribe(powerUps => {
      oWorld.powerUps = powerUps.map(powerUp => new PowerUp(powerUp.x, powerUp.y, powerUp.type));
    });

    this.socket.fromEvent<PowerUp>('consume').subscribe(powerUp => {
      const index = oWorld.powerUps.findIndex(p => p.x === powerUp.x && p.y === powerUp.y);
      oWorld.powerUps.splice(index, 1);
      oWorld.self.grow();
    });

    this.socket.fromEvent<string>('death').subscribe(playerId => {
      const offlinePlayer = oWorld.playerList.get(playerId);
      if (offlinePlayer && offlinePlayer.id === playerId) {
        offlinePlayer.alive = false;
      } else if (snake.id === playerId) {
        snake.alive = false;
        this.gameSerive.onStop.next();
        this.router.navigate(['/core/lost']);
      }
    });

    renderer.onUpdate = progress => {
      oWorld.update(progress);
    };

    renderer.onDraw = () => {
      oWorld.draw(renderer);
    };
  }
}
