import { Component } from '@angular/core';
import { trigger, state, style, transition, animate, group, query } from '@angular/animations';
import { GameService } from './core/services/game/game.service';
import { RouterOutlet } from '@angular/router';
import { slideVertical } from './shared/animations/slide/slide';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('openClosed', [
      // ...
      state('open', style({ opacity: 1 })),
      state(
        'closed',
        style({
          opacity: 0,
          display: 'none'
        })
      ),
      state('blur', style({ filter: 'blur(8px)' })),
      state('unblur', style({ filter: 'blur(0px)' })),
      transition('open => closed', [animate('350ms')]),
      transition('closed => open', [animate('250ms')]),
      transition('unblur => blur', [animate('350ms')]),
      transition('blur => unblur', [animate('250ms')])
    ]),

    trigger('routeAnimations', [transition(':increment', slideVertical('top')), transition(':decrement', slideVertical('bottom'))])
  ]
})
export class AppComponent {
  constructor(public gameService: GameService) {}

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}
