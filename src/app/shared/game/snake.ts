import { SnakePart } from './snake-part';
import { Renderer } from './renderer';
import * as Hammer from 'hammerjs';
import { Socket } from 'ngx-socket-io';

export class OnlinePlayer {
  public id: string;
  public x: number;
  public y: number;
  public direction: 0 | 1 | 2 | 3;
  public tailParts: SnakePart[];
  public alive: boolean;

  constructor(player: { id: string; x: number; y: number; direction: 0 | 1 | 2 | 3; tailParts: SnakePart[] }) {
    this.id = player.id;
    this.x = player.x;
    this.y = player.y;
    this.direction = player.direction;
    this.tailParts = player.tailParts;
  }

  draw(r: Renderer, size: number) {
    if (this.tailParts.length !== 0) {
      for (let i = 0; i < this.tailParts.length; i++) {
        const nextPart = this.tailParts[i + 1];
        this.tailParts[i].draw(r, size, nextPart);
      }
    }

    let head = r.headL;
    if (this.direction === 0) {
      head = r.headT;
    } else if (this.direction === 1) {
      head = r.headR;
    } else if (this.direction === 2) {
      head = r.headB;
    }

    r.drawTexture(head, this.x * size, this.y * size, size, size);
  }
}

export class Snake extends OnlinePlayer {
  public updateX: number;
  public updateY: number;

  constructor(
    id: string,
    x: number,
    y: number,
    direction: 0 | 1 | 2 | 3,
    tailParts: SnakePart[],
    public speed: number,
    public socket: Socket
  ) {
    super({ id, x, y, direction, tailParts });
    this.updateX = x;
    this.updateY = y;
    this.alive = true;
  }

  update(progress: number) {
    const prevX = this.x;
    const prevY = this.y;

    this.move(progress);

    if (prevX !== this.x || prevY !== this.y) {
      if (this.tailParts.length !== 0) {
        for (let i = this.tailParts.length - 1; i > 0; i--) {
          const prevPart = this.tailParts[i - 1];
          this.tailParts[i].update(prevPart.x, prevPart.y, prevPart.direction);
        }

        this.tailParts[0].update(prevX, prevY, this.direction);
      }

      this.socket.emit('c03', {
        x: this.x,
        y: this.y,
        direction: this.direction,
        tailParts: this.tailParts.map(part => {
          return { x: part.x, y: part.y, direction: part.direction };
        }),
      });
    }
  }

  move(progress: number) {
    if (this.direction === 0) {
      this.updateY -= this.speed * progress;
      this.y = Math.floor(this.updateY);
    } else if (this.direction === 1) {
      this.updateX += this.speed * progress;
      this.x = Math.floor(this.updateX);
    } else if (this.direction === 2) {
      this.updateY += this.speed * progress;
      this.y = Math.floor(this.updateY);
    } else if (this.direction === 3) {
      this.updateX -= this.speed * progress;
      this.x = Math.floor(this.updateX);
    }
  }

  grow() {
    const last = this.tailParts[this.tailParts.length - 1] || (this as any);
    this.tailParts.push(new SnakePart(last.x, last.y, last.direction));
  }

  addControls() {
    addEventListener('keydown', ev => {
      if (ev.keyCode === 87 && this.direction !== 2) {
        this.direction = 0;
      } else if (ev.keyCode === 65 && this.direction !== 1) {
        this.direction = 3;
      } else if (ev.keyCode === 83 && this.direction !== 0) {
        this.direction = 2;
      } else if (ev.keyCode === 68 && this.direction !== 3) {
        this.direction = 1;
      }
    });

    const hammer = new Hammer(document.body);
    hammer.get('swipe').set({ direction: Hammer.DIRECTION_ALL });
    hammer.on('swipeup', ev => {
      if (this.direction !== 2) {
        this.direction = 0;
      }
    });
    hammer.on('swipeleft', ev => {
      if (this.direction !== 1) {
        this.direction = 3;
      }
    });
    hammer.on('swipedown', ev => {
      if (this.direction !== 0) {
        this.direction = 2;
      }
    });
    hammer.on('swiperight', ev => {
      if (this.direction !== 3) {
        this.direction = 1;
      }
    });
  }
}
