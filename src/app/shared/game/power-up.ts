import { Renderer } from './renderer';

export class PowerUp {
  constructor(public x: number, public y: number, public type: 'grow' | 'speed') {}

  draw(renderer: Renderer, fieldSize: number) {
    renderer.drawRect(this.x * fieldSize, this.y * fieldSize, fieldSize, fieldSize, '#000');
  }
}
