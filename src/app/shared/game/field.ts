import { Renderer } from './renderer';
import { SnakePart } from './snake-part';
import { Snake } from './snake';

export class Field {
  snake: Snake = null;
  powerUp: any = null;
  tail: SnakePart = null;

  constructor() {}

  spawnPowerUp() {
    this.powerUp = 'test';
  }

  update() {
    this.snake = null;
    this.tail = null;
  }

  draw(r: Renderer, x: number, y: number, size: number) {
    if (this.powerUp) {
      r.drawTexture(r.powerUpTexture, x * size, y * size, size, size);
    }
  }
}
