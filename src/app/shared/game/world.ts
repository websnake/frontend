import { Snake, OnlinePlayer } from './snake';
import { Renderer } from './renderer';
import { PowerUp } from './power-up';

export class World {
  self: Snake;

  constructor(public width: number, public height: number, public playerList: Map<string, OnlinePlayer>, public powerUps: PowerUp[]) {}

  setSelf(self: Snake) {
    this.self = self;
  }

  public update(progress: number) {
    if (this.self.alive) {
      this.self.update(progress);
      this.checkBounds(this.self);
    }
  }

  public draw(renderer: Renderer) {
    renderer.drawRect(0, 0, renderer.size, renderer.size, '#fff');

    const fieldSize = renderer.size / this.width;

    for (let i = 0; i < this.height; i++) {
      for (let j = 0; j < this.width; j++) {
        renderer.drawRect(i * fieldSize, j * fieldSize, fieldSize, 1, '#000');
        renderer.drawRect(i * fieldSize, j * fieldSize, 1, fieldSize, '#000');
      }
    }

    this.powerUps.forEach(powerUp => powerUp.draw(renderer, fieldSize));

    this.playerList.forEach(player => {
      if (player.alive) {
        player.draw(renderer, fieldSize);
      }
    });

    if (this.self.alive) {
      this.self.draw(renderer, fieldSize);
    }
  }

  private checkBounds(snake: Snake) {
    if (snake.x < 0) {
      snake.x = snake.updateX = this.width - 1;
    } else if (snake.x >= this.width) {
      snake.x = snake.updateX = 0;
    }

    if (snake.y < 0) {
      snake.y = snake.updateY = this.height - 1;
    } else if (snake.y >= this.height) {
      snake.y = snake.updateY = 0;
    }
  }
}
