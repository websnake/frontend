export class Renderer {
  public headL = new Image();
  public headR = new Image();
  public headT = new Image();
  public headB = new Image();
  public torsoL = new Image();
  public torsoR = new Image();
  public torsoT = new Image();
  public torsoB = new Image();
  public edgeL = new Image();
  public edgeR = new Image();
  public edgeT = new Image();
  public edgeB = new Image();
  public tailEndL = new Image();
  public tailEndR = new Image();
  public tailEndT = new Image();
  public tailEndB = new Image();
  public powerUpTexture = new Image();

  private lastRender = 0;
  private ctx: CanvasRenderingContext2D;
  public size: number;

  constructor(canvas: HTMLCanvasElement, container: HTMLDivElement) {
    this.ctx = canvas.getContext('2d');
    this.setSize(canvas, container);
    window.onresize = () => this.setSize(canvas, container);
    window.requestAnimationFrame(time => this.draw(time));

    this.torsoL.src = 'assets/textures/torso_l.png';
    this.torsoR.src = 'assets/textures/torso_r.png';
    this.torsoT.src = 'assets/textures/torso_t.png';
    this.torsoB.src = 'assets/textures/torso_b.png';
    this.edgeL.src = 'assets/textures/edge_l.png';
    this.edgeR.src = 'assets/textures/edge_r.png';
    this.edgeT.src = 'assets/textures/edge_t.png';
    this.edgeB.src = 'assets/textures/edge_b.png';
    this.tailEndL.src = 'assets/textures/tailend_l.png';
    this.tailEndR.src = 'assets/textures/tailend_r.png';
    this.tailEndT.src = 'assets/textures/tailend_t.png';
    this.tailEndB.src = 'assets/textures/tailend_b.png';
    this.headL.src = 'assets/textures/snakehead_open_l.png';
    this.headR.src = 'assets/textures/snakehead_open_r.png';
    this.headT.src = 'assets/textures/snakehead_open_t.png';
    this.headB.src = 'assets/textures/snakehead_open_b.png';
    this.powerUpTexture.src = 'assets/textures/Powerup.png';
  }

  private draw(timestamp: number) {
    const progress = timestamp - this.lastRender;

    this.onUpdate(progress);
    this.onDraw();

    this.lastRender = timestamp;
    window.requestAnimationFrame(time => this.draw(time));
  }

  public drawRect(x: number, y: number, width: number, height: number, color: string) {
    this.ctx.fillStyle = color;
    this.ctx.fillRect(x, y, width, height);
  }

  public drawTexture(image: HTMLImageElement, x: number, y: number, width: number, height: number) {
    this.ctx.drawImage(image, x, y, width, height);
  }

  public onUpdate(progress: number): void {}
  public onDraw(): void {}

  private setSize(canvas: HTMLCanvasElement, container: HTMLDivElement) {
    if (container.clientHeight > container.clientWidth) {
      this.size = container.clientWidth;
    } else {
      this.size = container.clientHeight;
    }

    canvas.width = this.size;
    canvas.height = this.size;
  }
}
