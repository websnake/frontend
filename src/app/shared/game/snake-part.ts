import { Renderer } from './renderer';

export class SnakePart {
  constructor(public x: number, public y: number, public direction: 0 | 1 | 2 | 3) {}

  update(renderX: number, renderY: number, direction: 0 | 1 | 2 | 3) {
    this.x = renderX;
    this.y = renderY;
    this.direction = direction;
  }

  draw(r: Renderer, size: number, next: SnakePart) {
    let texture;

    if (!next) {
      switch (this.direction) {
        case 0:
          texture = r.tailEndT;
          break;
        case 1:
          texture = r.tailEndR;
          break;
        case 2:
          texture = r.tailEndB;
          break;
        case 3:
          texture = r.tailEndL;
          break;
      }
    } else if (next.direction !== this.direction) {
      if ((this.direction === 0 && next.direction === 3) || (this.direction === 1 && next.direction === 2)) {
        texture = r.edgeT;
      } else if ((this.direction === 3 && next.direction === 0) || (this.direction === 2 && next.direction === 1)) {
        texture = r.edgeL;
      } else if ((this.direction === 1 && next.direction === 0) || (this.direction === 2 && next.direction === 3)) {
        texture = r.edgeR;
      } else if ((this.direction === 3 && next.direction === 2) || (this.direction === 0 && next.direction === 1)) {
        texture = r.edgeB;
      }
    } else {
      switch (this.direction) {
        case 0:
          texture = r.torsoT;
          break;
        case 1:
          texture = r.torsoR;
          break;
        case 2:
          texture = r.torsoB;
          break;
        case 3:
          texture = r.torsoL;
          break;
      }
    }

    r.drawTexture(texture, this.x * size, this.y * size, size, size);
  }
}
