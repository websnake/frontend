import { style, query, group, animate } from '@angular/animations';

export function slideHorizontal(direction: 'left' | 'right') {
  return [
    style({
      position: 'relative',
      overflow: 'hidden'
    }),
    query(':enter, :leave', [
      style({
        position: 'absolute',
        width: 'calc(100% - var(--space) * 2)',
        height: 'calc(100% - var(--space) * 2)'
      })
    ]),
    query(':enter', [style({ transform: `translateX(${direction === 'left' ? '100vw' : '-100vw'})` })]),
    // Animate the new page in
    group([
      query(':enter', [animate('300ms ease', style({ transform: 'translateX(0)' }))]),
      query(':leave', [animate('300ms ease', style({ transform: `translateX(${direction === 'left' ? '-100vw' : '100vw'})` }))])
    ])
  ];
}

export function slideVertical(direction: 'top' | 'bottom') {
  return [
    style({
      position: 'relative',
      overflow: 'hidden'
    }),
    query(':enter, :leave', [
      style({
        position: 'absolute',
        width: 'calc(100%)',
        height: 'calc(100%)'
      })
    ]),
    query(':enter', [style({ transform: `translateY(${direction === 'top' ? '100vh' : '-100vh'})` })]),
    // Animate the new page in
    group([
      query(':enter', [animate('300ms ease', style({ transform: 'translateY(0)' }))]),
      query(':leave', [animate('300ms ease', style({ transform: `translateY(${direction === 'top' ? '-100vh' : '100vh'})` }))])
    ])
  ];
}
