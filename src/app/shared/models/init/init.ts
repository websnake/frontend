import { OnlinePlayer } from '../../game/snake';
import { SnakePart } from '../../game/snake-part';
import { PowerUp } from '../../game/power-up';

export class Init {
  self: { id: string; x: number; y: number; direction: 0 | 1 | 2 | 3; speed: number; tailParts: SnakePart[] };
  world: { width: number; height: number; powerUps: PowerUp[]; playerList: OnlinePlayer[] };
}
