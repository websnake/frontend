import { Component, OnInit } from '@angular/core';
import { GameService } from '../services/game/game.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(public game: GameService) {}

  ngOnInit() {}
}
