import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  readonly onStart = new Subject<GameConfig>();
  readonly onStop = new Subject<void>();
  showMenu = true;
  lastConfig: GameConfig;

  constructor() {
    this.onStart.subscribe(value => {
      this.lastConfig = value;
      this.showMenu = false;
    });

    this.onStop.subscribe(() => {
      this.showMenu = true;
    });
  }
}

interface GameConfig {
  type: 'singleplayer' | 'multiplayer';
  data: { difficulty: 'easy' | 'medium' | 'hard' } | { id: number };
}
